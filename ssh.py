#!/usr/bin/python
#Author Todor Yakimov
import paramiko

#Return an ssh paramiko object already connected 
#to a node that can be used for specifying commands 
#for execution
def sshConnect(ip):
  #SSH User credentials
  #-----------------
  userName = "root"
  password = "admin"
  #-----------------
  ssh = paramiko.SSHClient()
  ssh.load_system_host_keys()
  ssh.connect(ip, username=userName, password=password)
  return ssh
