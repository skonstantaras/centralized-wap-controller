#!/usr/bin/python
#Author Todor Yakimov
#Usage: collectSettings.py 192.168.1.1,192.168.1.4,192.168.1.5,... NOTE THAT THERE ARE NO SPACES b4 and after commas
import sys,re,math,pickle, random

from ssh import sshConnect
from operator import itemgetter
from util import convert_dbm_to_mw, calculate_distance_for_two_aps, adjust_tx_power
import json #Unneccessary imports used for auxiliary purposes such as formatting. TO BE REMOVED

#TODO: 
#DONE: equalize rx/tx readings.one is in mW and the other in dBm
#DONE: where is the 2nd 1sthop neigh???
#2ndHopNeighbor distance - unimplemented
#DOME: 1stHopNeigh rx distance complex numbers seen

#Notes
#lan interface (br0)'s IP can be deduced from user passed IP so the command "ifconfig br0" from previous versions has been removed


iwlist_surveyTable = dict()
def calculateOptimalChannel(channelNoise):
  #The function assumes the usage of 2.4Ghz channels and 802.11 OFDM spacing of 20MHz (b/g/n) usage of either b/g/n.
  channels = set([1,2,3,4,5,6,7,8,9,10,11])
  freeChannels= channels-set(channelNoise.keys())
  #First check whether optimal conditions exist and a space of free 20MHz exists
  if len(freeChannels) > 0:
    if set([1,2,3]).issubset(freeChannels):
      return 1
    elif set([4,5,6,7,8]).issubset(freeChannels):
      return 6
    elif set([9,10,11]).issubset(freeChannels):
      return 11
    #Reduce to +/- 3 channels offset
    for i in channels:
        #Generate needed free channels sequence [2],[1,2,3],[2,3,4],...,[9,10,11],[10]
        if i % 10 == 1:
          neededChannels = [2 * i % 12]
        else:
          neededChannels = [j for j in range(i-1,i+2)]
        if set(neededChannels).issubset(freeChannels) and i in list(freeChannels):
          return i
    #Otherwise just return a free channel
    return [i for i in channels if i not in channelNoise.keys()][0]
  #Otherwise return channel with least amount of noise
  noise = [sum(i) for i in channelNoise.values()]
  return noise.index(min(noise))+1
  
  
neighborTable = dict()
"""
The neighbor table has the following form:
{
  AP1_MAC:
    {
      LocalSettings:
        [
            IP,        
            SSID,
            Tx_Power, (in milliwatts  atm)
            Channel,  - in MHz
        ]
      LocalSettings_Optimal:
        [
            Channel,
            TxPower
        ]  
      1stHopNeighbor:
        {
          Neighbor1_MAC:
            [
                Channel,
                Rx_power, - in dbm atm
                IP,       - This value can only be inserted here after settings have been obtained from the local device to which the neighbor corresponds
                Distance  - The value has to be calculated and it's inserted after all other settings for every device have been obtained                          
            ],
          NeighborX_MAC:...
        }
      2stHopNeighbor: 
        {
          Neighbor1_MAC:
            [
                Channel,
                Rx_power, - (in dbm atm)
                IP,       - This value can only be inserted here after settings have been obtained from the local device to which the neighbor corresponds
                Distance  - The value has to be calculated and it's inserted after all other settings for every device have been 
                            obtained                               
            ],
          NeighborX_MAC:...
        }
    },
  APX_MAC:...
}
"""
#Build up the neighbor table in 3 steps:
for ip in sys.argv[1].strip().split(","): 
  #Get needed output and save it for processing
  ssh = sshConnect(ip)
  stdin, stdout, stderr = ssh.exec_command("iwconfig ath0")
  output = stdout.read()
  #Get local ssid
  localSSID = re.findall(r'ESSID:\".*\"\s', output)[0][7:-2]
  #Get local center frequency (channel)
  localChannel = float(re.findall(r'Frequency:.*\sAcc',output)[0][10:-9])
  #Get local mac
  localMAC = re.findall(r'Access\sPoint:\s.*\s',output)[0][14:-2].strip()
  #Get local IP
  stdin, stdout, stderr = ssh.exec_command("ifconfig br0")
  localIP = re.findall(r'inet addr:.*\s', stdout.read())[0].split(' ')[1][5:]
  #Get local transmit power
  stdin, stdout, stderr = ssh.exec_command("iwlist ath0 txpower")
  localTxPower = re.findall(r'Current\sTx-Power=.*\s',stdout.read())[0][:-1].split()[3][1:]
  #Populate "LocalSettings" section of table
  neighborTable[localMAC] = {"LocalSettings":[localIP, localSSID, int(localTxPower), float(localChannel)]}
# Step 1b: Get 1st hop neighbor via heard beacons
  stdin, stdout, stderr = ssh.exec_command("iwlist ath0 scanning")
  output = stdout.read()[27:-2].split('Cell')
  #-------------------------------
  #Step 3 interlude: do optimal settings detection:
  stdin, stdout, stderr = ssh.exec_command("site_survey")
  iwlist_surveyTable[localMAC] = stderr.read().split("\n")
  #-------------------------------
  #Identify which ones are in the same ESSID
  for neighborBeacon in output:
    if localSSID in neighborBeacon:
      #Get neighbor MAC
      neighborMAC = re.findall(r'\s-\sAddress:\s.*\s', neighborBeacon)[0][12:-1]
      #Get neighbor Rx transmit power(Beacon SSI)
      rxvalue = float(re.findall(r'Signal\slevel=.*\sN',neighborBeacon)[0][13:-7])
      #Get channel
      neighborChannel = float(re.findall(r'Frequency:.*\sGHz',neighborBeacon)[0][10:-4])
      #Partially populate neighbors section of each AP. 
      if not neighborTable[localMAC].get("1stHopNeighbor"):
        neighborTable[localMAC].update({"1stHopNeighbor":{neighborMAC:[neighborChannel, rxvalue]}})
      else:
        neighborTable[localMAC].get("1stHopNeighbor")[neighborMAC] = [neighborChannel, rxvalue]
  
#Step 2:
for localMAC in neighborTable.keys():
  for tier1Neigh in neighborTable.get(localMAC).get("1stHopNeighbor").items():
    #Step 2a: Insert IP addresses in each subtable
    if tier1Neigh[0] in neighborTable.keys():
      tier1Neigh[1].append(neighborTable.get(tier1Neigh[0]).get("LocalSettings")[0])
    #Step 2b: Insert distances in each tier 1 table
    pt = neighborTable.get(localMAC).get("LocalSettings")[2]
    pr = tier1Neigh[1][1]
    tier1Neigh[1].append(calculate_distance_for_two_aps(tier1Neigh[1][0],pt, convert_dbm_to_mw(pr), 5, 5))


#Step 4: partially populate 2ndHopNeighbor table leaving out distance
for apMAC in neighborTable.keys():
  #Get 1sthopNeighborMAC 
  for tier1NeighborMAC in neighborTable.get(apMAC).get("1stHopNeighbor").keys():
    #Get this 1stHopNeighbor's neighbors and check if any of them are not seen by current AP:
    neighborNeighbors = [i for i in neighborTable.get(tier1NeighborMAC).get("1stHopNeighbor").keys()]
    for mac in neighborNeighbors:
      if str(mac) not in str(neighborTable.get(apMAC).get("1stHopNeighbor").keys()) and str(mac) not in str(apMAC):
        channel = neighborTable.get(mac).get("LocalSettings")[3]
        ip = neighborTable.get(mac).get("LocalSettings")[0]
        rxpower = neighborTable.get(mac).get("LocalSettings")[2]
        neighborTable[apMAC].update({"2ndHopNeighbor":{mac:[channel, rxpower, ip]}})





#Step 5: calculate and populate optimum settings section
for apMAC, beacons in iwlist_surveyTable.items():
  #Calculate channel
  apSurvey = dict()
  for beacon in beacons:
    if beacon not in "":
      channel = int(beacon.split("] ")[3][8:].strip())
      noise = int(beacon.split("] ")[5][7:])
      if channel in apSurvey.keys():
        apSurvey[channel].append(noise)
      else:
        apSurvey[channel] = [noise]
  #Calculate power for devices on the same channel
  #Get all 1stHopNeigs that are on the same channel
  sameChannelNeigh = dict()
  for neighMAC, data in neighborTable.get(apMAC).get("1stHopNeighbor").items():
    localChannel = neighborTable.get(apMAC).get("LocalSettings")[3]
    remoteChannel = neighborTable.get(apMAC).get("1stHopNeighbor").get(neighMAC)[0]
    if float(localChannel) == float(remoteChannel):
      neighRx = neighborTable.get(apMAC).get("1stHopNeighbor").get(neighMAC)[1]
      sameChannelNeigh[neighMAC] = neighRx
    #For neigh with weakest signal use the adjust f-la.
  #In case there really are 1sthop devs on teh same channel, only then exec this part
  if len(sameChannelNeigh) != 0:
    centerFrequency = neighborTable.get(apMAC).get("LocalSettings")[3]
    weakestNeighMAC =   min(sameChannelNeigh, key=sameChannelNeigh.get)
    neighborRx = sameChannelNeigh.get(weakestNeighMAC)
    distance = neighborTable.get(apMAC).get("1stHopNeighbor").get(weakestNeighMAC)[3]
    adjustedPower = calculate_tx_power_by_distance(centerFrequency,distance/float(2), neighborRx)
  #Apply settings
  channel = calculateOptimalChannel(apSurvey)
  neighborTable[apMAC]["LocalSettings_Optimal"] = [channel]
  if len(sameChannelNeigh) != 0:
    neighborTable[apMAC]["LocalSettings_Optimal"].append(adjustedPower)
  else:
     neighborTable[apMAC]["LocalSettings_Optimal"].append(neighborTable[apMAC].get("LocalSettings")[2])
 
"""
#Apply settings to each AP
for apMAC, data in neighborTable.items():
  ip = data.get("LocalSettings")[0]
  channel = data.get("LocalSettings_Optimal")[0]
  txpower = data.get("LocalSettings_Optimal")[1]
  ssh = sshConnect(ip)
  stdin, stdout, stderr = ssh.exec_command("iwconfig ath0 channel "+str(channel))
  stdin, stdout, stderr = ssh.exec_command("iwconfig ath0 txpower  "+str(txpower))
"""

  
#Write neighborTable object to file for sharing it with the rest of the scripts
#filehandler = open("neighborTable.pythonObject","a+")
#pickle.dump(neighborTable,filehandler)

print json.dumps(neighborTable, indent=2)
