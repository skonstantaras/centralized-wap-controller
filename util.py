import math

def convert_dbm_to_mw(val):
  #mW =10^(dBm/10)I
  #That formula it's correct
  return 10**(val/10)

def convert_mw_to_dbm(val):
  #dbm = 10*log10(mw/1)
  #That formula it's correct
  x,y = math.modf(10*math.log10(val/1))
  return y

#Friis Transmission Formula. Antenna gain is taken as constant value of 1
def calculate_distance_for_two_aps(centerFrequency, pt, pr, gt, gr):
    #The implementation of the Friis Transmision formula
    #That formula it's correct
    #
    #pt is Power transmission in Mw
    #pr is Power received Mw
    #gt is Antenna gain sent in Mw
    #gr is Antenna gain receive Mw
    #speed of light is 300.000km/s
    #frequency in Hz (..not MHz)
    c = 300000000
    f = 1000000000 * centerFrequency

    gt = convert_dbm_to_mw(gt)
    gr = convert_dbm_to_mw(gr)
    part1 = float(c / (4 * math.pi * f))
    part2 = float(math.sqrt((pt * gt * gr) / pr))
    R = part1 * part2

    return round(R, 4)


def calculate_signal_loss_by_distance(centerFrequency,dist):
    #The Free Space Path Loss formula
    #That formula it's correct
    #
    #FSPL (dB) = 20log10(d)+ 20log10(f) + 32.44
    #where d is meters and f in Hz
    f= 1000 * centerFrequency  #From Ghz -> MHz
    d = dist * 0.001 #From meters > Km 

    part1 = 20*math.log10(d)
    part2 = 20*math.log10(f)
    result = part1 + part2 + 32.44 # result is dbm
    
    return result

def calculate_tx_power_by_margin(tx, rx, margin):
    #The method of Cross Multiplication
    #That formula it's correct
    #
    #Result is given in miliWatts
    new_rx= margin * math.fabs(rx)
    tx = convert_mw_to_dbm(tx)
    new_tx = (tx * new_rx)/math.fabs(rx)

    return convert_dbm_to_mw(new_tx)

def calculate_tx_power_by_distance(centerFrequency,dist,rx):
    #For a given distance we calculate the signal loss
    #and we obtain the Tx power according to the desired Rx
    #
    #Propability to be incorrect
    loss = calculate_signal_loss_by_distance(centerFrequency,dist)
    new_tx =  float(rx + loss)
    print new_tx, rx, loss
    return convert_dbm_to_mw(new_tx) 

def check_if_needs_optimization(rx):
    #Value 1 means needs optimization
    #Value 0 means it is not necessary
    #Value -1 means it is perfect
    if (rx < -80):
        return 1
    elif (rx < -60):
	return 0
    else:
	return -1

def adjust_tx_power(centerFrequency,tx,rx, distance):
   #Max Legal Tx Power is 27dbm or 501mw
   #Propability to be incorrect
   #
   tx_limit = float(501)
   improve_gain = float(1.2)#I want at least 20% reception improvement
   power_threshold = float(0.8)#Only if the signal is very bad we exceed this limit
   new_tx=0

   if (tx < tx_limit):
      if (1 == check_if_needs_optimization(rx)):
	#Reception signal is very bad and needs urgent optimization
	 new_tx = calculate_tx_power_by_margin(tx, rx, improve_gain)
      elif(0 == check_if_needs_optimization(rx) and tx < tx_limit*power_threshold):
        #If the chip is not transmiting close to the limit we can optimize the signal
         new_tx = calculate_tx_power_by_margin(tx, rx, improve_gain)

   if(new_tx > tx_limit):
      new_tx=tx_limit
   return round(new_tx,0)

