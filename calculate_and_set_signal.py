__author__ = 'skonstantaras'
import collections
import math

#ssid = 'testnet'

def convert_dbm_to_mw(val):
    #-10 dBm =	0.1000 mW
    # 0 dBm =	1.0000 mW
    # 1 dBm =	1.2589 mW
    #Consumer APs usually do not go over 501Mw
    # which is 27dBm

    return val * 1.258925


def calculate_distance_for_two_aps(centerFrequency, pt, pr, gt, gr):
    #The implementation of the Friis Transmision formula
    #
    #pt is Power transmission in Mw
    #pr is Power received Mw
    #gt is Antenna gain sent in Mw
    #gr is Antenna gain receive Mw
    #speed of light is 300.000km/s
    #frequency is 2400000000 Hz
    c = 300000000
    R = (c / (4 * math.pi * f)) * (math.sqrt((pt * gt * gr) / pr))
    return R

#print(neighbourTable)

def calculate_distance_for_away_aps(a1a2,a1a3,a2a3):
    #a1a2 is the distance between ap1 and ap2
    #a1a3 is the distance between ap1 and ap3
    #a2a3 is the distance between ap2 and ap3

    gamma = math.acos((math.pow(a1a2)+math.pow(a1a3)-math.pow(a2a3))/2*a1a2*a1a3)
    c2 = a1a2 + a1a3 - 2*a1a2*a1a3*math.cos(gamma)
    return c2


def create_neighborhoods():
    global neighbourTable


    for ap_mac in neighbourTable:

    closest_neighbor = None
    ap_entries = ap_mac.items
    ap_locals = None

    for item in ap_entries:
        if item.key == "LocalSettings":
            ap_locals = item
        else:
            if item.channel == ap_locals.channel and (
                    closest_neighbor == None or closest_neighbor.distance > item.distance):
                closest_neighbor = item

    if closest_neighbor == None:
        for mac_entry in neighbourTable:
            if mac_entry.key != ap_mac:
                mac_entry_items = mac_entry.items
                for item in mac_entry_items:
                    if item.key != "LocalSettings" and item.channel != ap_locals.channel:
                        distance = math.acos
                        if closest_neighbor == None or (closest_neighbor.distance > distance):
                            closest_neighbor = mac_entry
                            closest_neighbor.distance = distance

    if closest_neighbor == None:
        print("SetSignalStrengthMAX")
    else:
        print("SetSignalStrength")





